
// Опишите своими словами как работает цикл forEach.
// Ответ: метод forEach используется для перебора массива, вызывая при этом функцию callback для каждого элемента. Метод forEach передаёт функции три параметра: очередной элемент массива, его номер, массив, который перебирает.


function filterBy(array, typeOfArg) {
   
   
    return array.filter(item => {
        return typeof item !== typeOfArg
    });

}

const data = ['hello', 'world', 23, '23', null]

console.log(filterBy(data, "string"));
// console.log(filterBy(data, "number"));
// console.log(filterBy(data, null));


